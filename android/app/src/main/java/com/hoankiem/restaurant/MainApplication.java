package com.hoankiem.restaurant;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.content.Intent;


import com.reactnativenavigation.controllers.ActivityCallbacks;
import com.microsoft.codepush.react.CodePush;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.analytics.RNFirebaseAnalyticsPackage;
import io.sentry.RNSentryPackage;
import com.cmcewen.blurview.BlurViewPackage;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

import com.facebook.FacebookSdk;
import com.facebook.CallbackManager;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import io.underscope.react.fbak.RNAccountKitPackage;

import com.oblador.vectoricons.VectorIconsPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import cl.json.RNSharePackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.rnfingerprint.FingerprintAuthPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.airbnb.android.react.maps.MapsPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import org.reactnative.camera.RNCameraPackage;
import com.horcrux.svg.SvgPackage;
import com.rt2zz.reactnativecontacts.ReactNativeContacts;
import com.airbnb.android.react.lottie.LottiePackage;
import com.remobile.toast.RCTToastPackage;
import com.masteratul.exceptionhandler.ReactNativeExceptionHandlerPackage;

import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.config.RNFirebaseRemoteConfigPackage;
import io.invertase.firebase.crash.RNFirebaseCrashPackage;
import io.invertase.firebase.instanceid.RNFirebaseInstanceIdPackage;
import io.invertase.firebase.links.RNFirebaseLinksPackage;

public class MainApplication extends Application implements ReactApplication {

    private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

    protected static CallbackManager getCallbackManager() {
        return mCallbackManager;
    }

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public String getJSBundleFile() {
          return CodePush.getJSBundleFile();
        }
    
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }
  
        @Override
        protected List<ReactPackage> getPackages() {
        return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new CodePush(BuildConfig.ANDROID_CODEPUSH_DEPLOYMENT_KEY, MainApplication.this, BuildConfig.DEBUG),
            new ReactNativeConfigPackage(),
            new RNSentryPackage(MainApplication.this),
            new BlurViewPackage(),
            new RNFirebasePackage(),
            new RNFirebaseAnalyticsPackage(),
            new VectorIconsPackage(),
            new LinearGradientPackage(),
            new RNSharePackage(),
            new RNDeviceInfo(),
            new FingerprintAuthPackage(),
            new PickerPackage(),
            new MapsPackage(),
            new RNGoogleSigninPackage(),
            new FBSDKPackage(mCallbackManager),
            new RNAccountKitPackage(),
            new RNCameraPackage(),
            new SvgPackage(),
            new ReactNativeContacts(),
            new LottiePackage(),
            new RCTToastPackage(),
            new ReactNativeExceptionHandlerPackage(),
            new RNFirebaseNotificationsPackage(),
            new RNFirebaseMessagingPackage(),
            new RNFirebaseRemoteConfigPackage(),
            new RNFirebaseCrashPackage(),
            new RNFirebaseInstanceIdPackage(),
            new RNFirebaseLinksPackage()
            );
        }
    };

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public ReactNativeHost getReactNativeHost() {
      return mReactNativeHost;
    }
  
    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }
}
