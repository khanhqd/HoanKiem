require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "RNGoogleSignin"
  s.version      = package["version"]
  s.summary      = "Google Signin for your react native applications"
  s.homepage     = "https://github.com/devfd/react-native-google-signin"
  s.license      = "MIT"
  s.author             = { "devfd" => "apptailor.co@gmail.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://github.com/devfd/react-native-google-signin.git", :tag => "#{s.version}" }

  s.source_files  = "ios/**/*.{h,m}"

  s.dependency "React"
end
