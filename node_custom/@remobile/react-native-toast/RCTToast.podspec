require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "RCTToast"
  s.version      = package["version"]
  s.summary      = "A android like toast for react-native support for ios and android"
  s.homepage     = "https://github.com/remobile/react-native-toast"
  s.license      = "MIT"
  s.author             = { "YunJiang.Fang" => "42550564@qq.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://github.com/remobile/react-native-toast.git", :tag => "#{s.version}" }

  s.source_files  = "ios/**/*.{h,m}"

  s.dependency "React"
end
