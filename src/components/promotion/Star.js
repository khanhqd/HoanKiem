import React, { Component } from 'react';
import { Image, View } from 'react-native';
import PropTypes from 'prop-types';

export default class Star extends Component {
  render() {
    let { point } = this.props;
    return (
      <View style={styles.container}>
        <Image
          style={point >= 1 ? styles.star : styles.starInact}
          source={require('@assets/HK/icons/star.png')}
        />
        <Image
          style={point >= 2 ? styles.star : styles.starInact}
          source={require('@assets/HK/icons/star.png')}
        />
        <Image
          style={point >= 3 ? styles.star : styles.starInact}
          source={require('@assets/HK/icons/star.png')}
        />
        <Image
          style={point >= 4 ? styles.star : styles.starInact}
          source={require('@assets/HK/icons/star.png')}
        />
        <Image
          style={point >= 5 ? styles.star : styles.starInact}
          source={require('@assets/HK/icons/star.png')}
        />
      </View>
    );
  }
}
Star.propTypes = {
  point: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};
const styles = {
  container: {
    flexDirection: 'row',
  },
  star: {
    width: 11,
    height: 11,
    resizeMode: 'contain',
    marginLeft: 4,
  },
  starInact: {
    width: 11,
    height: 11,
    resizeMode: 'contain',
    tintColor: '#9BA9BB',
    marginLeft: 4,
  },
};
