import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { COLOR } from 'uikit';

export default class DetailItem extends Component {
  render() {
    let {
      content, date, price,
    } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.titleContent}>{content}</Text>
        <Text style={styles.descrip}>Áp dụng đến: {date}</Text>
        <View style={{ flexDirection: 'row', paddingTop: 16, alignItems: 'center' }}>
          <Image
            source={require('@assets/HK/icons/ic_voucher.png')}
            style={styles.icon}
          />
          <Text style={styles.price}>{price}Đ</Text>
        </View>
      </View>
    );
  }
}

DetailItem.propTypes = {
  content: PropTypes.string,
  date: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  price: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

const styles = StyleSheet.create({
  container: {
    padding: 16,
    borderBottomWidth: 1,
    borderColor: 'rgb(222,225,227)',
    backgroundColor: 'white',
  },
  descrip: {
    fontSize: 13,
    color: '#9BA9BB',
    paddingTop: 4,
  },
  titleContent: {
    fontSize: 17,
    fontWeight: '700',
    color: '#24272B',
    paddingBottom: 8,
  },
  icon: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
    tintColor: COLOR.MAIN,
    marginRight: 7,
  },
  price: {
    fontSize: 17,
    fontWeight: '700',
    color: '#223549',
  },
});
