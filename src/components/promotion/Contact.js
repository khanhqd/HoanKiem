import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import PropTypes from 'prop-types';

export default class Contact extends Component {
  render() {
    let { address, phone } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Liên hệ</Text>
        <Text style={styles.address}>{address}</Text>
        <View style={styles.row}>
          <Image
            style={styles.icon}
            source={require('@assets/HK/icons/phone.png')}
          />
          <Text>{phone}</Text>
        </View>
      </View>
    );
  }
}
Contact.propTypes = {
  address: PropTypes.string,
  phone: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};
const styles = {
  container: {
    padding: 16,
    backgroundColor: 'white',
  },
  title: {
    fontSize: 17,
    fontWeight: '700',
    color: '#24272B',
  },
  address: {
    fontSize: 15,
    color: '#3E4A59',
    paddingVertical: 16,
    lineHeight: 24,
  },
  phone: {
    fontSize: 15,
    fontWeight: '700',
    color: '#3E4A59',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginRight: 16,
  },
};
