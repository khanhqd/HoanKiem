import React, { Component } from 'react';
import { Image, View, Dimensions } from 'react-native';
import Swiper from 'react-native-swiper';

const width = Dimensions.get('window').width;

export default class ImageThumb extends Component {
  render() {
    return (
      <Swiper
        style={styles.wrapper}
        dotStyle={styles.dot}
        activeDotStyle={styles.activeDot}
      >
        <View style={styles.slide1}>
          <Image
            style={styles.image}
            source={{ uri: 'https://danang.huongnghiepaau.com/images/ga-nuong-xa-xiu.png' }}
          />
        </View>
        <View style={styles.slide2}>
          <Image
            style={styles.image}
            source={{ uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ' }}
          />
        </View>
        <View style={styles.slide3}>
          <Image
            style={styles.image}
            source={{ uri: 'https://cdn-image.foodandwine.com/sites/default/files/styles/medium_2x/public/201010-xl-big-italian-salad.jpg?itok=1zvxLIZ7' }}
          />
        </View>
      </Swiper>
    );
  }
}

const styles = {
  wrapper: {
    height: width * 0.5,
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  image: {
    width,
    height: width * 0.65,
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  dot: {
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: 'white',
  },
  activeDot: {
    backgroundColor: 'white',
  },
};
