import React, { Component } from 'react';
import { Image, Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
export default class ImageBig extends Component {
  render() {
    return (
      <Image
        style={styles.container}
        source={{ uri: this.props.uri }}
      />
    );
  }
}

const styles = {
  container: {
    width: (width - 32) / 2,
    height: ((width - 32) / 2) * 0.65,
    resizeMode: 'cover',
  },
};
