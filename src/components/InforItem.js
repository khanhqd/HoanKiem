import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { COLOR } from 'uikit';

export default class InforItem extends Component {
  render() {
    return (
      <View>
        <View style={styles.row}>
          <Image
            source={this.props.icon}
            style={styles.icon}
          />
          <Text style={styles.title}>{this.props.title}</Text>
        </View>
        <Text style={styles.descrip}>{this.props.descrip}</Text>
      </View>
    );
  }
}

const styles = {
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 6,
  },
  icon: {
    width: 23,
    height: 23,
    resizeMode: 'contain',
    marginRight: 5,
  },
  title: {
    fontSize: 14,
    fontWeight: '500',
    color: COLOR.SUB_TEXT,
  },
  descrip: {
    fontSize: 14,
    fontWeight: '500',
    color: COLOR.MAIN_TEXT,
  },
};
