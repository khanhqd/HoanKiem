import React, { Component } from 'react';
import { Text, TouchableOpacity } from 'react-native';

export default class ButtonM extends Component {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        style={[styles.container, { backgroundColor: this.props.backgroundColor }]}
      >
        <Text style={[styles.title, { color: this.props.color }]}>{this.props.title}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = {
  container: {
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
  },
  title: {
    fontSize: 17,
  },
};
