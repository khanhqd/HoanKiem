import React from 'react';
import { Dimensions, Image, Text, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { STYLE, COLOR } from 'uikit';

const width = Dimensions.get('window').width;

const FoodItem = props => {
  let {
    onPress, uri, name, price,
  } = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={styles.container}
    >
      <View style={styles.containerImg}>
        <Image
          style={styles.img}
          source={{ uri }}
        />
      </View>
      <View style={styles.contentContainer}>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.price}>{price} $</Text>
      </View>
    </TouchableOpacity>
  );
};

FoodItem.propTypes = {
  onPress: PropTypes.func,
  uri: PropTypes.string,
  name: PropTypes.string,
  price: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

const styles = {
  container: {
    width: Math.min(177, (width / 2) * 0.9),
    marginRight: 16,
    backgroundColor: COLOR.BG,
  },
  img: {
    width: Math.min(177, (width / 2) * 0.9),
    height: Math.min(177, (width / 2) * 0.9) * 0.65,
    borderRadius: 3,
    backgroundColor: STYLE.COLOR.BG,
    resizeMode: 'cover',
  },
  contentContainer: {
    backgroundColor: 'white',
    padding: 12,
  },
  name: {
    color: COLOR.MAIN_TEXT,
    fontSize: 16,
    fontWeight: '500',
  },
  price: {
    fontSize: 13,
    color: COLOR.SUB_TEXT,
  },
};

export default FoodItem;
