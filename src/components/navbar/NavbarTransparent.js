import React from 'react';
import { View, TouchableOpacity, Image, Text, Platform, Dimensions } from 'react-native';
import { Base, commonPropType } from 'uikit';

class NavbarTransparent extends Base {

  static propTypes = {
    children: commonPropType.children,
  }

  render() {
    let {
      iconLeft, textLeft, title, textRight, iconRight, onPressRight, onPressLeft, boldText, badge,
    } = this.props;
    return (
      <View>
        {Platform.OS === 'ios' && <View style={styles.statusBar} />}
        <View style={styles.container}>
          <TouchableOpacity activeOpacity={0.6} style={styles.btnContainer} onPress={onPressLeft}>
            {iconLeft ?
              <Image source={iconLeft} style={styles.icon} />
              :
              <Text style={boldText ? styles.boldText : styles.textIcon}>{textLeft}</Text>
            }
          </TouchableOpacity>
          <View style={styles.titleContainer}>
            <Text style={[styles.title, { color: this.props.color }]}>{title}</Text>
          </View>
          {typeof (onPressRight) === 'function' ?
            <TouchableOpacity activeOpacity={0.6} style={styles.btnContainer} onPress={onPressRight}>
              {iconRight ?
                <Image source={iconRight} />
                :
                <Text style={boldText ? styles.boldText : styles.textIcon}>{textRight}</Text>
              }
              {(badge && badge > 0) ?
                <View style={styles.badgeContainer}>
                  <Text style={styles.badgeNumber}>{String(badge)}</Text>
                </View>
                :
                <View />
              }
            </TouchableOpacity>
            :
            <View style={[styles.btnContainer, { width: 45 }]} />
          }
        </View>
      </View>
    );
  }
}

const styles = {
  statusBar: {
    backgroundColor: 'transparent',
    height: Dimensions.get('window').height === 812 ? 40 : 20,
  },
  container: {
    flexDirection: 'row',
    height: 50,
    padding: 10,
    backgroundColor: 'transparent',
    alignItems: 'center',
  },
  btnContainer: {
    padding: 5,
    paddingHorizontal: 10,
  },
  titleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  icon: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    tintColor: 'white',
  },
  title: {
    fontSize: 17,
    fontWeight: '600',
    marginLeft: 10,
    // color: 'white',
  },
  textIcon: {
    fontSize: 15,
    color: 'white',
  },
  boldText: {
    fontSize: 17,
    fontWeight: '600',
    color: 'white',
  },
  badgeContainer: {
    width: 18,
    height: 18,
    borderRadius: 9,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5AE23',
    position: 'absolute',
    top: 4,
    right: 4,
  },
  badgeNumber: {
    fontSize: 11,
    color: 'white',
  },
};

export default NavbarTransparent;

