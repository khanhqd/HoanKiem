import React from 'react';
import { Dimensions, Image, Text, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { STYLE, COLOR } from 'uikit';

const width = Dimensions.get('window').width;

const PromotionItem = props => {
  let {
    onPress, uri, content,
  } = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={styles.container}
    >
      <View style={styles.containerImg}>
        <Image
          style={styles.img}
          source={{ uri }}
        />
      </View>
      <Text style={styles.content} numberOfLines={2}>{String(content).trim()}</Text>
    </TouchableOpacity>
  );
};

PromotionItem.propTypes = {
  onPress: PropTypes.func,
  uri: PropTypes.string,
  content: PropTypes.string,
};

const styles = {
  container: {
    width: Math.min(294, width * 0.75),
    marginRight: 16,
    backgroundColor: COLOR.BG,
  },
  containerImg: {
    marginBottom: 16,
  },
  img: {
    width: Math.min(294, width * 0.75),
    height: (Math.min(294, width * 0.75)) * 0.65,
    borderRadius: 3,
    backgroundColor: STYLE.COLOR.BG,
    resizeMode: 'cover',
  },
  content: {
    fontSize: 16,
    color: COLOR.MAIN_TEXT,
    fontWeight: '500',
    lineHeight: 24,
  },
};

export default PromotionItem;
