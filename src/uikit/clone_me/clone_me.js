import React, { Fragment } from 'react';
import { Base, withGlobalProps, commonPropType } from '../index';

class CloneMe extends Base {

  static propTypes = {
    children: commonPropType.children,
  }

  render() {
    return (
      <Fragment />
    );
  }

}

export default withGlobalProps(CloneMe);
