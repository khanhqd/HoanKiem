import React from 'react';
import PropTypes from 'prop-types';
import { COLOR } from './index';

const styleProperties = ['alignContent', 'alignItems', 'alignSelf', 'aspectRatio', 'backfaceVisibility', 'backgroundColor', 'borderBottomColor', 'borderBottomLeftRadius', 'borderBottomRightRadius', 'borderBottomWidth', 'borderColor', 'borderLeftColor', 'borderLeftWidth', 'borderRadius', 'borderRightColor', 'borderRightWidth', 'borderStyle', 'borderTopColor', 'borderTopLeftRadius', 'borderTopRightRadius', 'borderTopWidth', 'borderWidth', 'bottom', 'color', 'decomposedMatrix', 'direction', 'display', 'elevation', 'flex', 'flexBasis', 'flexDirection', 'flexGrow', 'flexShrink', 'flexWrap', 'fontFamily', 'fontSize', 'fontStyle', 'fontVariant', 'fontWeight', 'height', 'includeFontPadding', 'justifyContent', 'left', 'letterSpacing', 'lineHeight', 'margin', 'marginBottom', 'marginHorizontal', 'marginLeft', 'marginRight', 'marginTop', 'marginVertical', 'maxHeight', 'maxWidth', 'minHeight', 'minWidth', 'opacity', 'overflow', 'overlayColor', 'padding', 'paddingBottom', 'paddingHorizontal', 'paddingLeft', 'paddingRight', 'paddingTop', 'paddingVertical', 'position', 'resizeMode', 'right', 'rotation', 'scaleX', 'scaleY', 'shadowColor', 'shadowOffset', 'shadowOpacity', 'shadowRadius', 'textAlign', 'textAlignVertical', 'textDecorationColor', 'textDecorationLine', 'textDecorationStyle', 'textShadowColor', 'textShadowOffset', 'textShadowRadius', 'tintColor', 'top', 'transform', 'transformMatrix', 'translateX', 'translateY', 'width', 'writingDirection', 'zIndex'];

export const commonPropType = {
  children          : PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  style             : PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
};

const commonStyleSheet = {
  bgMain: {
    backgroundColor: COLOR.MAIN,
  },
  bgWhite: {
    backgroundColor: COLOR.WHITE,
  },
  bold: {
    fontWeight: 'bold',
  },
  colorMain: {
    color: COLOR.MAIN,
  },
  colorWhite: {
    color: COLOR.WHITE,
  },
  middle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  flex1: {
    flex: 1,
  },
};

export const styleSet = (name) => {
  if (!!commonStyleSheet[name]) return commonStyleSheet[name];
  return {};
};

export const _s = styleSet; // for short

const hasNumber = (myString) => {
  return /\d/.test(myString);
};

export const propsToStyle = (props = {}) => {
  let style = {};
  for (let key in props) {
    if (styleProperties.indexOf(key) !== -1) style[key] = props[key];
    else if (hasNumber(key)) { // make prop style with number, flex1 -> flex: 1
      let matchArr = key.match(/\d+/g);
      if (matchArr != null && matchArr.length === 1 && key.indexOf(matchArr[0]) === key.length - matchArr[0].length) {
        const numberValue = Number(matchArr[0]);
        const propertyValue = key.substring(0, key.indexOf(matchArr[0]));
        const styleObject = { [propertyValue]: numberValue };
        style = Object.assign(style, styleObject);
      }
    }
  }
  for (let key in commonStyleSheet) {
    if (!!props[key]) style = Object.assign(style, commonStyleSheet[key]);
  }
  return style;
};

export const withGlobalProps = (Component) => {

  const isStateless = !Component.prototype.render;
  if (isStateless) {
    return (props) => {
      return <Component {...props} style={Object.assign(propsToStyle(props), props.style)} />;
    };
  }
  return class NewComponent extends Component {

    render() {
      return React.cloneElement(super.render(), {
        style: Object.assign(propsToStyle(this.props), this.props.style),
      });
    }
  };
};
