import React from 'react';
import { View } from 'react-native';
import { Base, withGlobalProps, commonPropType, Button } from '../index';

class Col extends Base {

  static propTypes = {
    children: commonPropType.children,
  }

  render() {
    const { onPress, children } = this.props;
    return typeof onPress === 'function' ? (
      <Button onPress={onPress} activeOpacity={0.9} {...this.props}>
        { children }
      </Button>
    ) : (
      <View {...this.props}>
        { children }
      </View>
    );
  }

}

export default withGlobalProps(Col);
