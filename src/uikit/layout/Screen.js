// import React from 'react';
// import firebase from 'react-native-firebase';
import { StackActions } from 'react-navigation';
import { Dimensions } from 'react-native';
import { Base } from '../index';

export default class Screen extends Base {

  getProps = () => {
    if (!this.props.navigation.state || !this.props.navigation.state.params) return {};
    return this.props.navigation.state.params;
  }

  pop = () => this.props.navigation.goBack();

  popTo = (key) => this.props.navigation.goBack({ routeName: key });

  popToRoot = () => this.props.navigation.popToTop();

  popMultiScreen = (num) => {
    const popAction = StackActions.pop({
      n: num,
    });
    this.props.navigation.dispatch(popAction);
  }
  // popToRoot = () => this.props.navigator.popToRoot({
  //   animated: true,
  // });

  push = (screenName, args = {}) => {
    // firebase.analytics().setCurrentScreen(screenName, screenName);
    return this.props.navigation.navigate(screenName, args);
  }

  showModal = (screenName, options = {}) => {
    // firebase.analytics().setCurrentScreen(screenName, screenName);
    return this.props.navigator.showModal({
      screen: screenName,
      animated: true,
      animationType: 'fade',
      ...options,
    });
  }

  closePopup = () => {
    this.props.navigation.navigate('Main');
  }

  onScroll = ({ nativeEvent }) => {
    let windowHeight = Dimensions.get('window').height;
    let height = nativeEvent.contentSize.height;
    let offset = nativeEvent.contentOffset.y;
    if (windowHeight + offset > height + 20) {
      if (this.state.isLoading || this.state.refreshing || !this.state.canLoadMore) return;
      this.loadMore();
    }
  }

}

