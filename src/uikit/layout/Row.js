import React from 'react';
import { Base, Col, commonPropType } from '../index';

class Row extends Base {

  static propTypes = {
    children: commonPropType.children,
  }

  render() {
    return <Col flexDirection="row" alignItems="center" {...this.props} />;
  }

}

export default Row;
