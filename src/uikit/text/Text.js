import React from 'react';
import { Text } from 'react-native';
import { Base, withGlobalProps, commonPropType, COLOR } from '../index';

class TextDefault extends Base {

  static propTypes = {
    children: commonPropType.children,
  }

  render() {
    const textStyle = {
      color: COLOR.MAIN_TEXT,
      backgroundColor: 'transparent',
    };
    return (
      <Text {...this.props} style={[textStyle, this.props.style]} />
    );
  }

}

export default withGlobalProps(TextDefault);
