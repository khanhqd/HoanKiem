import React from 'react';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// export default {
//   FontAwesome, Entypo, EvilIcons, Ionicons, MaterialIcons, SimpleLineIcons, MaterialCommunityIcons,
// };

const VectorIcon = (props) => {
  let vectorIcon;
  switch (props.provider) {
    case 'FontAwesome': vectorIcon = <FontAwesome {...props} />; break;
    case 'Entypo': vectorIcon = <Entypo {...props} />; break;
    case 'EvilIcons': vectorIcon = <EvilIcons {...props} />; break;
    case 'Ionicons': vectorIcon = <Ionicons {...props} />; break;
    case 'MaterialIcons': vectorIcon = <MaterialIcons {...props} />; break;
    case 'SimpleLineIcons': vectorIcon = <SimpleLineIcons {...props} />; break;
    case 'MaterialCommunityIcons': vectorIcon = <MaterialCommunityIcons {...props} />; break;
    default: return null;
  }
  return vectorIcon;
};

export default VectorIcon;
