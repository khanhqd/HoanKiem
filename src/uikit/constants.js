import { Platform, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export let COLOR = {
  GREY: '#C7C7CD',
  RED: '#FF0000',
  WHITE: '#fff',
  YELLOW: '#ffff00',
  GREEN: '#449D47',
  PURPLE: '#551A8B',
  BLUE: '#2196F3',
  NEW_BLUE: '#12579E',
  MAIN_TEXT: '#333333',
  SUB_TEXT: '#9E9E9E',
  SUB_TEXT_LIGHT: 'rgb(213,215,217)',
  MAIN: 'rgb(255,140,43)',
  SUB_COLOR: 'rgb(174,174,174)',
  BG: 'rgb(247,247,247)',
  BG_GREY: '#f4f3f9',
  BORDER: 'rgb(234,234,236)',
  TITLE_BOX: '#6D6D72',
};

export let STYLE = {
  WIDTH: width, // Platform.isTablet ? width*0.55 : width,
  HEIGHT: height,
  HEADER_HEIGHT: Platform.OS === 'ios' ? 44 : 64,
  BORDERWIDTH: 1,
  COLOR,
};

export default STYLE;
