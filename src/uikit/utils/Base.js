import { Component } from 'react';

export default class Base extends Component {

  componentDidMount() {
    this._mounted = true;
    typeof this.didMount === 'function' && this.didMount();
  }

  componentWillUnmount() {
    this._mounted = false;
    typeof this.willUnmount === 'function' && this.willUnmount();
  }

  setState = (objOrFunc, callback) => new Promise((resolve) => {
    if (this._mounted === false) return resolve();
    super.setState(objOrFunc, () => {
      if (typeof callback === 'function') {
        return callback();
      }
      return resolve();
    });
  });
}
