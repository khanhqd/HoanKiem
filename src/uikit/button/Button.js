import React from 'react';
import { TouchableOpacity, Platform } from 'react-native';
import Ripple from 'react-native-material-ripple';
import { Base, withGlobalProps, commonPropType } from '../index';
import preventDoublePress from './preventDoublePress';

const DEFAULT_OPACITY = 0.6;

class Button extends Base {

  static propTypes = {
    children: commonPropType.children,
  }

  render() {
    const { style, activeOpacity, children } = this.props;
    return Platform.OS === 'ios' ? (
      <TouchableOpacity activeOpacity={activeOpacity || DEFAULT_OPACITY} onPress={this.handlePress} style={style}>
        {children}
      </TouchableOpacity>
    ) : (
      <Ripple onPress={this.handlePress} style={style} rippleDuration={300} rippleColor="rgba(0,0,0,0.2)">
        {children}
      </Ripple>
    );
  }

  handlePress = () => preventDoublePress(this.props.onPress)

}

export default withGlobalProps(Button);
