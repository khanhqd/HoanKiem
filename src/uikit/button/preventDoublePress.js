let _pressTimeout;
const DURATION_PREVENT = 100; // miliseconds

export default (func) => {
  if (_pressTimeout) clearTimeout(_pressTimeout);
  if (typeof func !== 'function') return;
  _pressTimeout = setTimeout(() => func(), DURATION_PREVENT);
};
