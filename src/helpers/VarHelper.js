// import _ from "lodash"; // for handle array https://lodash.com/docs
// const v = require("voca"); // for handle string https://vocajs.com/
import moment from 'moment';

const _ = require('lodash');

class VarHelper {

  dotNotate = (obj, _target, _prefix) => {
    let target = _target || {};
    let prefix = _prefix || '';
    Object.keys(obj).forEach((key) => {
      if (typeof obj[key] === 'object') {
        this.dotNotate(obj[key], target, `${prefix}${key}.`);
      } else {
        target[prefix + key] = obj[key];
      }
    });
    return target;
  }

  /*
    let foo = { c: { b: 1 } };
    let bar =  this.dot(foo);
    console.log(bar["c.b"]) // -> 1

    really helpful for TextInput Wrapper
  */
  dot = obj => this.dotNotate(obj, undefined, undefined);
  noDuplicateArray = arr => _.uniqWith(arr, _.isEqual);

  uniqueArr(array, key) {
    let newArr;
    if (key) newArr = _.uniqBy(array, key);
    else {
      newArr = _.uniqBy(array, (e) => {
        return e;
      });
    }
    return newArr;
  }
  secondToMinute = (time) => {
    let minute = Math.floor(time / 60);
    let second = time - (minute * 60);
    if (String(minute).length === 1) minute = `0${minute}`;
    if (String(second).length === 1) second = `0${second}`;
    return `${minute}:${second}`;
  }
  numberFormat = (num) => {
    if (!num) return '';
    let newNum = `${num.toString()}`;
    newNum = newNum.replace(/\./g, '');
    let _dot = '.';
    let parts = newNum.toString().split(_dot);
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, _dot);
    let foo = parts.join(_dot);
    return foo;
  }

  moneyFormat = (num) => {
    if (!num) return '0';
    let newNum = num.toString();
    newNum = newNum.replace(/\./g, '');
    let _dot = '.';
    let parts = newNum.toString().split(_dot);
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, _dot);
    let foo = parts.join(_dot);
    return foo;
  }

  moneyTextToValue = (num) => {
    return `${num}000`;
  }

  convertPhone(phone) {
    if (phone.charAt(0) === '0') {
      return phone.slice(1, phone.length);
    }
    return phone;
  }

  cleanNumber(numberString) {
    let newNumber = numberString || '';
    newNumber = newNumber.replace('+84', '0');
    newNumber = newNumber.replace(new RegExp('[-]', 'g'), '');
    newNumber = newNumber.replace(new RegExp('[*]', 'g'), '');
    newNumber = newNumber.replace(new RegExp('[#]', 'g'), '');
    newNumber = newNumber.replace(new RegExp('[(]', 'g'), '');
    newNumber = newNumber.replace(new RegExp('[)]', 'g'), '');
    newNumber = newNumber.replace(new RegExp('[.]', 'g'), '');
    newNumber = newNumber.replace(new RegExp('[,]', 'g'), '');
    newNumber = newNumber.replace(new RegExp('[ ]', 'g'), '');
    newNumber = newNumber.replace(/\s/g, '');
    if (newNumber.substr(0, 2) === '00') {
      let new2;
      new2 = `0${newNumber.substr(2)}`;
      newNumber = new2;
    }
    let numberic = '0123456789';
    let arr = newNumber.split('');
    let result = '';
    for (let i = 0; i < arr.length; i++) {
      if (numberic.indexOf(arr[i]) !== -1) {
        result += String(arr[i]);
      }
    }
    if (result === '') return result;
    return result;
  }

  getTransTypeById = (type) => {
    switch (type) {
      case 'TRANSFER':
        return 'Chuyển tiền';
      case 'WITHDRAW':
        return 'Giao dịch rút tiền';
      case 'GIFT':
        return 'Nhận tiền chuyển';
      case 'TOPUP':
        return 'Nạp tiền điện thoại';
      case 'BUYCARD':
        return 'Mua thẻ';
      case 'PAYMENT':
        return 'Thanh toán hoá đơn';
      case 'REFUND':
        return 'Hoàn tiền';
      case 'CONVERT':
        return 'Chuyển về tài khoản chính';
      case 'DEPOSIT':
        return 'Giao dịch nạp tiền';
      default:
        return type;
    }
  }

  formatTime = (value, formatOutput) => {
    return moment(value).format(formatOutput);
  }

}

export default new VarHelper();

