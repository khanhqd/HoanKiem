// import LocalizedStrings from 'react-localization';
// import { AsyncStorage } from "react-native";
// import 'core-js/es6/symbol';
// import 'core-js/fn/symbol/iterator';
// import  { startTabApp } from "./start-app";

// const strings = {
//   vi: {
//     home: "Trang chủ",
//     discover: "Khám phá",
//     news: "Tin tức",
//     notification: "Thông báo",
//     settings: "Cài đặt"
//   },
//   en: {
//     home: "Home",
//     discover: "Discover",
//     news: "News",
//     notification: "Notification",
//     settings: "Settings"
//   }
// };

// let wordings = new LocalizedStrings(strings);
// wordings.setLanguage("vi");

// export const getCurrentLang = async () => {
//   let currentLang = await AsyncStorage.getItem("APP_LANGUAGE");
//   if(currentLang !== 'vi' && currentLang !== 'en') {
//     currentLang = 'vi';
//   }
//   return currentLang
// }

// export const switchLang = async code => {
//   await AsyncStorage.setItem("APP_LANGUAGE", code);
//   await AsyncStorage.setItem("APP_LOGIN_CASE", "TAB");
//   wordings.setLanguage(code);
//   startTabApp();
// }

// export const combineLanguages = async code => {
//   let newStrings = Object.assign({}, strings);
//   let lang = code.lang;
//   let string = {
//     vi: { ...newStrings.vi, ...lang.wordings.vi },
//     en: { ...newStrings.en, ...lang.wordings.vi },
//   };
//   // mutate require("../../lang") để có gợi ý
//   wordings = new LocalizedStrings(newStrings);
//   lang.lang = wordings;
//   let currentLang = await getCurrentLang();
//   wordings.setLanguage(currentLang);
// }

// export default wordings;
