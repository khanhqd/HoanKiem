import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings([
  'Warning: Overriding previous layout animation',
  'source.uri should not be an empty string',
  'Method `jumpToIndex` is deprecated',
  'Warning: isMounted(...) is deprecated',
  'Class RCTCxxModule was not exported',
  'Module RCTImageLoader requires main queue setup',
  'Class WhatsAppShare was not exported. Did you forget to use RCT_EXPORT_MODULE()?',
  'Class GooglePlusShare was not exported. Did you forget to use RCT_EXPORT_MODULE()?',
  'Class GenericShare was not exported. Did you forget to use RCT_EXPORT_MODULE()?',
  'Module RNGoogleSignin requires main queue setup since it overrides `constantsToExport` but doesn\'t implement `requiresMainQueueSetup`. In a future release React Native will default to initializing all native modules on a background thread unless explicitly opted-out of.',
]);
