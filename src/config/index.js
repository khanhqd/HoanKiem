import {
  config,
  ReactNative,
} from 'libs';
import env from '../config.env';

// Combine native config and generated JS config
export default {
  ...config,
  ...env,
  ...ReactNative.NativeModules.RNUeno,
  __native: config,
  __js: env,
};
