import { COLOR } from 'uikit';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

// define your suffixes by yourself..
// here we use active, big, small, very-big..
const replaceSuffixPattern = /--(active|big|small|very-big)/g;
// const PRIMARY_COLOR = () => STYLE.COLOR.HIGHLIGHT;
const iconsFactory = (PRIMARY_COLOR) => ({
  'heart-o': [25, PRIMARY_COLOR, FontAwesome],
  heart: [25, PRIMARY_COLOR, FontAwesome],
  'star-o': [30, PRIMARY_COLOR, FontAwesome],
  star: [30, PRIMARY_COLOR, FontAwesome],
  'ios-search': [32, PRIMARY_COLOR, Ionicons],
  'play-circle-outline': [30, PRIMARY_COLOR, MaterialIcons],
  menu: [30, PRIMARY_COLOR, Entypo],
  news: [30, PRIMARY_COLOR, Entypo],
  key: [15, PRIMARY_COLOR, FontAwesome],
  user: [25, PRIMARY_COLOR, FontAwesome],
  'user-o': [25, PRIMARY_COLOR, FontAwesome],
  phone: [25, PRIMARY_COLOR, MaterialIcons],

});

const defaultIconProvider = EvilIcons;

let iconsMap = {};
let iconsLoaded = new Promise((resolve) => {
  const icons = iconsFactory(COLOR.MAIN);
  new Promise.all(
    Object.keys(icons).map(iconName => {
      const Provider = icons[iconName][2] || defaultIconProvider;
      return Provider.getImageSource(
        !!icons[iconName][3] ? icons[iconName][3].replace(replaceSuffixPattern, '') : iconName.replace(replaceSuffixPattern, ''),
        icons[iconName][0],
        icons[iconName][1],
      );
    })).then(sources => {
    Object.keys(icons).forEach((iconName, idx) => {
      iconsMap[iconName] = sources[idx];
    });

    resolve(true);
  });
});

export { iconsMap, iconsLoaded };
