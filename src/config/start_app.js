import React, { Component } from 'react';
import { Alert } from 'react-native';
import { Provider } from 'mobx-react/native';
import store from 'stores';
import { setJSExceptionHandler, setNativeExceptionHandler } from 'react-native-exception-handler';
import { codePush } from 'libs';
import { HoanKiem } from '../screens/nav';
import './ignore_warning';

const errorHandler = (e) => {
  let name;
  let message;
  try {
    name = e.name;
    message = e.message;
  } catch (err) {
    console.log(err);
  }
  Alert.alert(
    'Đã gặp sự cố',
    `
    ${name}: ${message}
    `,
    [{
      text: 'Thoát',
      onPress: () => { },
    },
    {
      text: 'Khởi động lại',
      onPress: () => { codePush.restartApp(); },
    }],
  );
};
setJSExceptionHandler(errorHandler, false);

setNativeExceptionHandler(() => {
  codePush.restartApp();
});

const getCurrentRouteName = (navigationState) => {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  if (route.routes) {
    return getCurrentRouteName(route);
  }
  return route.routeName;
};

export default class App extends Component {
  componentWillMount = async () => {
    await store.setup();
  }
  render() {
    return (
      <Provider {...store}>
        <HoanKiem
          onNavigationStateChange={(prevState, currentState) => {
            const currentScreen = getCurrentRouteName(currentState);
            const prevScreen = getCurrentRouteName(prevState);
            if (prevScreen !== currentScreen) {
              // store.systemStore.updateCurrentScreen(currentScreen);
            }
          }}
        />
      </Provider>
    );
  }
}
