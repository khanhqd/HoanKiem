import React from 'react';
import { View, Text, Platform, Dimensions, ScrollView, Image } from 'react-native';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';
import { Screen, COLOR } from 'uikit';
import Promotion from './Components/Promotion';
import NewProduct from './Components/NewProduct';
import HotCombo from './Components/HotCombo';
import { SCREENS } from '..';

const TabIcon = ({ tintColor }) => (
  <Image
    source={require('@assets/HK/tabbar/ic_home.png')}
    style={{ width: 23, height: 23, tintColor }}
    resizeMode="contain"
  />
);

@inject('testStore') @observer
export default class HomeHK extends Screen {

  static navigationOptions = () => ({
    // tabBarLabel: 'Tiền mặt',
    tabBarIcon: TabIcon,
  })

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  }
  product = [
    {
      uri: 'https://cdn-image.foodandwine.com/sites/default/files/styles/medium_2x/public/201010-xl-big-italian-salad.jpg?itok=1zvxLIZ7',
      title: 'KHAI VỊ',
      descrip: 'Pasta Salad',
      price: '8.9',
    },
    {
      uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
      title: 'TRÁNG MIỆNG',
      descrip: 'Pancake',
      price: '8.9',
    },
  ]
  hotCombo = [
    {
      uri: 'https://danang.huongnghiepaau.com/images/ga-nuong-xa-xiu.png',
      title: 'Mẹt gà nướng',
      price: '8.9',
      people: '4',
    },
    {
      uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
      title: 'Pancake',
      price: '8.9',
      people: '4',
    },
  ]
  toDetail = () => {
    this.push(SCREENS.FOOD_DETAIL);
  }
  render() {
    return (
      <View style={{ backgroundColor: COLOR.BG, flex: 1 }}>
        {Platform.OS === 'ios' && <View style={styles.statusBar} />}
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          style={{ backgroundColor: COLOR.BG, flex: 1 }}
        >
          <View style={styles.container}>
            <View style={{ paddingBottom: 20 }}>
              <Text style={styles.title}>GIỚI THIỆU</Text>
              <Promotion
                parent={this}
              />
            </View>
            <View>
              <Text style={styles.title}>MÓN MỚI</Text>
              <View>
                {this.product.map((item, index) => {
                  return (
                    <NewProduct
                      key={index}
                      title={item.title}
                      descrip={item.descrip}
                      price={item.price}
                      uri={item.uri}
                      onPress={this.toDetail}
                    />
                  );
                })}
              </View>
            </View>
            <View>
              <Text style={styles.title}>HOT COMBO</Text>
              <View>
                {this.hotCombo.map((item, index) => {
                  return (
                    <HotCombo
                      key={index}
                      uri={item.uri}
                      title={item.title}
                      price={item.price}
                      people={item.people}
                      onPress={this.toDetail}
                    />
                  );
                })}
              </View>
            </View>
          </View>
        </ScrollView>
        <LinearGradient
          style={styles.shadow}
          colors={['rgba(224,224,224,0)', 'rgba(224,224,224,0.2)', 'rgba(224,224,224,0.4)', 'rgba(224,224,224,0.6)', 'rgba(224,224,224,0.8)']}
        />
      </View>
    );
  }
}

const styles = {
  container: {
    backgroundColor: COLOR.BG,
    flex: 1,
    marginBottom: 16,
  },
  statusBar: {
    backgroundColor: 'transparent',
    height: Dimensions.get('window').height === 812 ? 40 : 20,
  },
  title: {
    fontSize: 14,
    color: COLOR.MAIN_TEXT,
    paddingLeft: 16,
    paddingTop: 16,
  },
  shadow: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    height: 15,
  },
};
