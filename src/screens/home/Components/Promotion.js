import React from 'react';
import { ScrollView, Dimensions } from 'react-native';
import { PromotionItem } from 'components';
import { Screen } from 'uikit';
import { SCREENS } from '../..';

const width = Dimensions.get('window').width;

export default class Promotion extends Screen {
  data = [
    {
      uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQr_tOdFKbl6HwjrG1ZDIxC98vJferbybcwUkGNnJWjGahj88Lp',
      content: 'Giới thiệu về quán, CTKM...',
      screen: SCREENS.PROMOTION_DETAIL,
    },
    {
      uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
      content: 'Món mới',
      screen: SCREENS.PROMOTION_DETAIL,
    },
    {
      uri: 'https://danang.huongnghiepaau.com/images/ga-nuong-xa-xiu.png',
      content: 'HOT COMBO',
      screen: SCREENS.PROMOTION_DETAIL,
    },
  ]

  toScreen = (screen) => {
    this.props.parent.push(screen);
  }
  render() {
    return (
      <ScrollView
        horizontal
        snapToAlignment="start"
        snapToInterval={Math.min(294, width * 0.75) + 17}
        showsHorizontalScrollIndicator={false}
        keyboardShouldPersistTaps="always"
        decelerationRate="fast"
        style={{ paddingTop: 9, paddingLeft: 16, marginRight: 16 }}
      >
        {this.data.map((item, index) => {
          return (
            <PromotionItem
              key={index}
              uri={item.uri}
              logo={item.logo}
              title={item.title}
              content={item.content}
              date={item.date}
              onPress={() => { this.toScreen(item.screen); }}
            />
          );
        })}
      </ScrollView>
    );
  }
}

