import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { ImageBig } from '../../../components';

export default class HotCombo extends Component {
  render() {
    let {
      title, price, uri, people, onPress,
    } = this.props;
    return (
      <TouchableOpacity
        onPress={onPress}
        style={styles.container}
      >
        <ImageBig
          uri={uri}
        />
        <View style={styles.contentContainer}>
          <Text style={styles.title}>{title}</Text>
          <View style={styles.row}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image
                style={styles.icon}
                source={require('@assets/HK/icons/ic_dollar.png')}
              />
              <Text style={styles.price}>{price}</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image
                style={styles.icon}
                source={require('@assets/HK/tabbar/ic_people.png')}
              />
              <Text style={styles.price}>{people} People</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
const styles = {
  container: {
    marginHorizontal: 16,
    marginTop: 18,
    backgroundColor: 'white',
    borderRadius: 3,
    overflow: 'hidden',
  },
  contentContainer: {
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: 16,
    color: '#333333',
    fontWeight: '500',
    lineHeight: 24,
  },
  price: {
    fontSize: 13,
    color: '#9E9E9E',
    lineHeight: 24,
  },
  icon: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    marginRight: 6,
  },
};
