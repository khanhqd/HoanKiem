import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { ImageBig } from '../../../components';

export default class NewProduct extends Component {
  render() {
    let {
      title, descrip, price, uri, onPress,
    } = this.props;
    return (
      <TouchableOpacity
        onPress={onPress}
        style={styles.container}
      >
        <ImageBig
          uri={uri}
        />
        <View style={styles.contentContainer}>
          <Text style={styles.title}>{title}</Text>
          <View style={styles.row}>
            <Text style={styles.descrip}>{descrip}</Text>
            <Text style={styles.price}>{price}$</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
const styles = {
  container: {
    marginHorizontal: 16,
    marginTop: 18,
    backgroundColor: 'white',
    borderRadius: 3,
    overflow: 'hidden',
  },
  contentContainer: {
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 10,
    lineHeight: 24,
    color: '#9E9E9E',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  descrip: {
    fontSize: 16,
    color: '#333333',
    fontWeight: '500',
    lineHeight: 24,
  },
  price: {
    fontSize: 13,
    color: '#9E9E9E',
    lineHeight: 24,
  },
};
