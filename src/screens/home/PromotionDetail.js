import React from 'react';
import { ImageThumb, DetailItem, Contact } from 'components';
import { Screen } from 'uikit';
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native';


export default class PromotionDetail extends Screen {

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <TouchableOpacity
          onPress={() => { this.pop(); }}
          style={styles.btnLeft}
        >
          <Image
            source={require('@assets/HK/navbar/ic_back.png')}
            style={styles.iconLeft}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.btnRight}>
          <Image
            source={require('@assets/HK/icons/ic_share_white.png')}
            style={styles.iconRight}
          />
        </TouchableOpacity>
        <ScrollView >
          <ImageThumb />
          <DetailItem
            logo={require('@assets/HK/icons/sumo.png')}
            title="Sumo BBQ"
            descrip="Buffet - Restaurant"
            point={4.7}
            rating={250}
            content="Giảm 20% Buffe Sumo BBQ"
            date="20/10/2018"
            price="100.000"
          />
          <Contact
            address="165- 167 Nguyễn Đức Cảnh, Phường Tân Phong, Quận 7 .TP Hồ Chí Minh"
            phone="+84 9555 6555"
          />
          <View style={styles.line} />
          <View style={styles.detail}>
            <Text style={styles.headerTitle}>Thông tin chi tiết</Text>
            <Text style={styles.detailTitle}>Gà là sự lựa chọn quen thuộc khi khách đến chơi nhà và thịt gà dù được chế biến thành món nào cũng đều mang lại sự thơm ngon, hấp dẫn, vô cùng bổ dưỡng. Mỗi món ăn lại chứa đựng mùi vị riêng biệt, tạo nên cảm giác khác nhau, khiến bạn không khỏi thích thú. Hôm nay, Adayroi.com sẽ cùng bạn đến với Nhà hàng Hoa viên Sơn Thủy để thưởng thức mẹt gà siêu ưu đãi với 01 con gà ta 1,5kg được chế biến thành 06 món bao gồm: Nướng muối ớt, chiên nước mắm, hấp, gỏi, cháo, lòng xào mướp.</Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  line: {
    height: 16,
    backgroundColor: 'rgb(246,248,250)',
    borderColor: 'rgb(222,225,227)',
    borderTopWidth: 1,
  },
  detail: {
    padding: 16,
  },
  headerTitle: {
    fontSize: 17,
    fontWeight: '700',
    color: '#24272B',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  iconLeft: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    tintColor: 'white',
  },
  iconRight: {
    width: 22,
    height: 22,
    resizeMode: 'contain',
  },
  btnLeft: {
    padding: 5,
    position: 'absolute',
    top: 27,
    left: 11,
    zIndex: 2,
  },
  btnRight: {
    padding: 5,
    position: 'absolute',
    top: 27,
    right: 11,
    zIndex: 2,
  },
  detailTitle: {
    fontSize: 15,
    color: '#3E4A59',
    paddingVertical: 16,
    lineHeight: 24,
  },
};
