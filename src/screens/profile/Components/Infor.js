import React, { Component } from 'react';
import { View } from 'react-native';
import { InforItem } from 'components';

export default class Infor extends Component {
  render() {
    return (
      <View>
        <InforItem
          icon={require('@assets/HK/icons/ic_home.png')}
          title="Địa chỉ"
          descrip="117 Phạm Ngọc Khánh - tp Hải Dương - tỉnh Hải Dương"
        />
        <View style={{ marginVertical: 23 }}>
          <InforItem
            icon={require('@assets/HK/icons/ic_duration.png')}
            title="Mở cửa"
            descrip="Monday - Sunday --- 8:00 am - 10: pm"
          />
        </View>
        <InforItem
          icon={require('@assets/HK/icons/ic_phone.png')}
          title="Liên hệ"
          descrip="0936746253 - 0967286357"
        />
      </View>
    );
  }
}
