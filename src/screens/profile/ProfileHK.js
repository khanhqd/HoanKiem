import React from 'react';
import { View, Platform, Dimensions, Image, ScrollView } from 'react-native';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { Screen, COLOR } from 'uikit';
import { ButtonM } from 'components';
import { MapView } from 'libs';
import LinearGradient from 'react-native-linear-gradient';
import Infor from './Components/Infor';

const TabIcon = ({ tintColor }) => (
  <Image
    source={require('@assets/HK/tabbar/ic_profile.png')}
    style={{ width: 23, height: 23, tintColor }}
    resizeMode="contain"
  />
);

@inject('testStore') @observer
export default class MenuHK extends Screen {
  static navigationOptions = () => ({
    // tabBarLabel: 'Tiền mặt',
    tabBarIcon: TabIcon,
  })
  static propTypes = {
    navigation: PropTypes.object.isRequired,
  }
  state = {
    region: {
      latitude: 37.78825,
      longitude: -122.4324,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    },
  }
  render() {
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <View style={styles.statusBar} />}
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
        >
          <View style={styles.mapContainer}>
            <MapView
              style={{ flex: 1, borderRadius: 5 }}
              region={this.state.region}
              onRegionChange={this.onRegionChange}
            />
          </View>
          <View style={styles.contentContainer}>
            <Infor />
          </View>
          <View style={styles.containerButton}>
            <View style={{ flex: 1, marginRight: 8 }}>
              <ButtonM
                color={COLOR.MAIN}
                backgroundColor="white"
                title="ĐẶT BÀN"
              />
            </View>
            <View style={{ flex: 1, marginLeft: 8 }}>
              <ButtonM
                color="white"
                backgroundColor={COLOR.MAIN}
                title="CHỈ ĐƯỜNG"
              />
            </View>
          </View>
        </ScrollView>
        <LinearGradient
          style={styles.shadow}
          colors={['rgba(224,224,224,0)', 'rgba(224,224,224,0.2)', 'rgba(224,224,224,0.4)', 'rgba(224,224,224,0.6)', 'rgba(224,224,224,0.8)']}
        />
      </View>
    );
  }
}

const styles = {
  container: {
    backgroundColor: COLOR.BG,
    flex: 1,
    paddingHorizontal: 16,
  },
  statusBar: {
    backgroundColor: 'transparent',
    height: Dimensions.get('window').height === 812 ? 40 : 20,
  },
  title: {
    fontSize: 14,
    color: '#333333',
    paddingLeft: 16,
    paddingTop: 16,
  },
  mapContainer: {
    backgroundColor: 'white',
    height: 205,
    borderRadius: 5,
    marginTop: 10,
  },
  contentContainer: {
    borderRadius: 5,
    backgroundColor: 'white',
    padding: 16,
    marginTop: 16,
  },
  containerButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    marginTop: 30,
  },
  shadow: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    height: 15,
  },
};
