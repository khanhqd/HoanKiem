import { StackNavigator, TabNavigator } from 'react-navigation';
import { Platform } from 'react-native';
import { COLOR } from 'uikit';
import { COMPONENT } from './index';

const TabScreen = TabNavigator({
  home: { screen: COMPONENT.HOME_HK },
  menu: { screen: COMPONENT.MENU_HK },
  profile: { screen: COMPONENT.PROFILE_HK },
}, {
  swipeEnabled: true,
  tabBarPosition: 'bottom',
  tabBarOptions: {
    showLabel: false,
    animationEnabled: true,
    showIcon: true,
    activeTintColor: COLOR.MAIN,
    inactiveTintColor: COLOR.SUB_COLOR,
    indicatorStyle: {
      backgroundColor: 'white',
    },
    style: {
      backgroundColor: 'white',
      borderWidth: 0,
      marginBottom: 0,
      height: Platform.OS === 'ios' ? 50 : 50,
    },
  },
},
);

const Main = StackNavigator({
  Splash: { screen: COMPONENT.SPLASH },
  Home: {
    screen: TabScreen,
    navigationOptions: {
      gesturesEnabled: false,
    },
  },
  FoodDetail:                 { screen: COMPONENT.FOOD_DETAIL },
}, {
  headerMode: 'none',
});

export const HoanKiem = StackNavigator({
  Main: { screen: Main },
  PromotionDetail: { screen: COMPONENT.PROMOTION_DETAIL },
  Popup:                      { screen: COMPONENT.SPLASH },
}, {
  headerMode: 'none',
  mode: 'modal',
  cardStyle: {
    backgroundColor: 'rgba(62,74,89,0.5)',
  },
  transitionConfig: () => ({ screenInterpolator: disableStackBackground }),
});

const disableStackBackground = (props) => {
  const { layout, position, scene } = props;
  const index = scene.index;
  const height = layout.initHeight;
  const translateX = 0;
  const translateY = position.interpolate({
    inputRange: ([index - 1, index, index + 1]),
    outputRange: ([height, 0, 0]),
  });
  return {
    transform: [{ translateX }, { translateY }],
  };
};
