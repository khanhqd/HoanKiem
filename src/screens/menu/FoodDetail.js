import React from 'react';
import { View, Image, Text, Dimensions, ScrollView } from 'react-native';
import { Screen, COLOR } from 'uikit';
import { NavbarTransparent } from 'components';

const width = Dimensions.get('window').width;

export default class FoodDetail extends Screen {
  data = [
    {
      material: 'cà chua, khoai tây, hành lá, thì là, xà lách, cóc, xoài, ổi',
    },
    {
      material: 'thịt bò, thịt gà, thịt lợn, thịt chó, tôm, cua, ốc, ếch',
    },
    {
      material: 'cà chua, khoai tây, hành lá, thì là, xà lách, cóc, xoài, ổi',
    },
    {
      material: 'cà chua, khoai tây, hành lá, thì là, xà lách, cóc, xoài, ổi',
    },
    {
      material: 'thịt bò, thịt gà, thịt lợn, thịt chó, tôm, cua, ốc, ếch',
    },
    {
      material: 'thịt bò, thịt gà, thịt lợn, thịt chó, tôm, cua, ốc, ếch',
    },
  ]
  render() {
    return (
      <ScrollView style={styles.container}>
        <Image
          source={{ uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ' }}
          style={styles.image}
        />
        <View style={{ position: 'absolute', left: 0, top: 0 }}>
          <NavbarTransparent
            iconLeft={require('@assets/HK/navbar/ic_back.png')}
            onPressLeft={() => this.pop()}
          />
        </View>
        <View style={styles.topTitleContainer}>
          <Text style={styles.topTitle}>Pasta Salad</Text>
        </View>
        <View style={styles.contentContainer}>
          <Text style={styles.contentTitle}>Miến trộn Hàng Thiếc với ngan chặt từng khúc vuông vắn kèm măng tươi giòn sật, rưới thêm chút xì dầu đủ để có thể khiến bạn no đến tận tối. Đặc biệt, nước chấm đậm đà với tỏi ớt băm nhuyễn, đường và nước mắm pha vừa đủ, chấm cùng không có gì để chê</Text>
        </View>
        <View style={styles.bottomTitleContainer}>
          <Text style={styles.bottomTitle}>NGUYÊN LIỆU</Text>
        </View>
        <View style={{ backgroundColor: 'white', paddingBottom: 16 }}>
          {this.data.map((item, index) => {
            return (
              <View
                key={index}
                style={styles.row}
              >
                <View style={styles.circle}>
                  <Text style={styles.number}>{index + 1}</Text>
                </View>
                <Text style={styles.material}>{item.material}</Text>
              </View>
            );
          })}
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  container: {
    backgroundColor: COLOR.BG,
    flex: 1,
  },
  image: {
    width,
    height: width / 2,
    resizeMode: 'cover',
  },
  topTitleContainer: {
    paddingHorizontal: 16,
    paddingTop: 16,
    paddingBottom: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  topTitle: {
    fontSize: 20,
    color: COLOR.MAIN_TEXT,
    fontWeight: '700',
  },
  contentContainer: {
    backgroundColor: 'white',
    paddingHorizontal: 32,
    paddingTop: 25,
    paddingBottom: 32,
  },
  contentTitle: {
    fontSize: 14,
    color: COLOR.MAIN_TEXT,
    lineHeight: 24,
  },
  bottomTitleContainer: {
    padding: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomTitle: {
    fontSize: 14,
    lineHeight: 24,
    color: COLOR.MAIN_TEXT,
  },
  row: {
    flexDirection: 'row',
    paddingVertical: 16,
    paddingLeft: 16,
    paddingRight: 30,
    alignItems: 'center',
  },
  circle: {
    width: 18,
    height: 18,
    borderRadius: 9,
    backgroundColor: COLOR.MAIN,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 8,
  },
  number: {
    color: 'white',
    fontSize: 10,
  },
  material: {
    fontSize: 14,
    color: COLOR.MAIN_TEXT,
    lineHeight: 24,
  },
};
