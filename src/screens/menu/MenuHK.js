import React from 'react';
import { View, Image, ScrollView } from 'react-native';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { Screen, COLOR } from 'uikit';
import { NavbarTransparent, ButtonM } from 'components';
import LinearGradient from 'react-native-linear-gradient';
import Food from './Components/Food';
import { SCREENS } from '..';

const TabIcon = ({ tintColor }) => (
  <Image
    source={require('@assets/HK/tabbar/ic_people.png')}
    style={{ width: 23, height: 23, tintColor }}
    resizeMode="contain"
  />
);

@inject('testStore') @observer
export default class MenuHK extends Screen {

  static navigationOptions = () => ({
    // tabBarLabel: 'Tiền mặt',
    tabBarIcon: TabIcon,
  })

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  }

  state = {
    apptezier: true,
    breakfast: false,
    dessert: false,
    salad: false,
    soup: false,
  }
  data = [
    {
      uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
      name: 'salad',
      price: '8.9',
    },
    {
      uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
      name: 'salad',
      price: '8.9',
    },
    {
      uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
      name: 'salad',
      price: '8.9',
    },
  ]
  menu = [
    {
      title: 'APPTEZIER',
      onPress: () => { this.setState({ apptezier: !this.state.apptezier }); },
      state: 'apptezier',
      data: [
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
      ],
    },
    {
      title: 'BREAKFAST AND BRUNCH',
      onPress: () => { this.setState({ breakfast: !this.state.breakfast }); },
      state: 'breakfast',
      data: [
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
      ],
    },
    {
      title: 'DESSERT',
      onPress: () => { this.setState({ dessert: !this.state.dessert }); },
      state: 'dessert',
      data: [
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
      ],
    },
    {
      title: 'SALAD',
      onPress: () => { this.setState({ salad: !this.state.salad }); },
      state: 'salad',
      data: [
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
      ],
    },
    {
      title: 'SOUP',
      onPress: () => { this.setState({ soup: !this.state.soup }); },
      state: 'soup',
      data: [
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
        {
          uri: 'https://d3awvtnmmsvyot.cloudfront.net/api/file/6fTZjAw9Tjqf4XrddmRQ',
          name: 'salad',
          price: '8.9',
          onPress: () => { this.push(SCREENS.FOOD_DETAIL); },
        },
      ],
    },
  ]
  render() {
    return (
      <View style={styles.container}>
        <NavbarTransparent
          title="MENU"
          color={COLOR.MAIN_TEXT}
          iconRight={require('@assets/HK/navbar/ic_search.png')}
          onPressRight={() => { this.push(SCREENS.FOOD_DETAIL); }}
        />
        <ScrollView
          keyboardShouldPersistTaps="always"
          showsVerticalScrollIndicator={false}
          style={styles.contentContainer}
        >
          <View style={{ marginBottom: 30 }}>
            {this.menu.map((item, index) => {
              return (
                <View
                  key={index}
                  style={{ paddingTop: 8 }}
                >
                  <ButtonM
                    title={item.title}
                    onPress={item.onPress}
                    backgroundColor="white"
                    color={COLOR.MAIN_TEXT}
                  />
                  {
                    this.state[item.state] === true &&
                    <View style={{ paddingBottom: 12 }}>
                      <Food
                        data={item.data}
                      />
                    </View>
                  }
                </View>
              );
            })}
          </View>
        </ScrollView>
        <LinearGradient
          style={styles.shadow}
          colors={['rgba(224,224,224,0)', 'rgba(224,224,224,0.2)', 'rgba(224,224,224,0.4)', 'rgba(224,224,224,0.6)', 'rgba(224,224,224,0.8)']}
        />
      </View>
    );
  }
}

const styles = {
  container: {
    backgroundColor: COLOR.BG,
    flex: 1,
  },
  contentContainer: {
    paddingHorizontal: 16,
    paddingTop: 10,
  },
  shadow: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    height: 15,
  },
};
