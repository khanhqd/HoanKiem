import React, { Component } from 'react';
import { ScrollView, Dimensions } from 'react-native';
import { FoodItem } from 'components';

const width = Dimensions.get('window').width;

export default class Food extends Component {
  render() {
    return (
      <ScrollView
        horizontal
        snapToAlignment="start"
        snapToInterval={Math.min(177, (width / 2) * 0.9) + 17}
        showsHorizontalScrollIndicator={false}
        keyboardShouldPersistTaps="always"
        decelerationRate="fast"
        style={{ paddingTop: 13 }}
      >
        {this.props.data.map((item, index) => {
          return (
            <FoodItem
              key={index}
              uri={item.uri}
              name={item.name}
              price={item.price}
              onPress={item.onPress}
            />
          );
        })}
      </ScrollView>
    );
  }
}
