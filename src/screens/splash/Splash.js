import React from 'react';
import { Platform, View, Image, Text } from 'react-native';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { Screen, COLOR } from 'uikit';
import { codePush } from 'libs';

@inject('testStore') @observer
export default class Splash extends Screen {

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  }

  state = {
    message: 'Checking update...',
    page: 'null',
  }

  didMount = () => {
    this.checkCodePushUpdate();
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('@assets/bg.png')}
          style={styles.bg}
        />
        <View style={{ position: 'absolute', top: 119, left: 31 }}>
          <Text style={styles.title}>HOAN KIEM</Text>
        </View>
        <View style={{ position: 'absolute', top: 159, right: 5 }}>
          <Text style={styles.descrip}>Restaurent</Text>
        </View>
      </View>
    );
  }

  showApp = async () => {
    // this.push(SCREENS.HOME_HK);
    setTimeout(() => {
      this.push('home');
    }, 1000);
  }

  checkCodePushUpdate = () => {
    let didShowApp = false;
    // let installMode = Platform.OS === 'ios' ? codePush.InstallMode.IMMEDIATE : codePush.InstallMode.ON_NEXT_RESTART;
    let keyTest = Platform.OS === 'ios' ? 'meF8Rlh6TytDvE4AvrKfuKExW1U68e630342-df63-4670-806f-293452da19c5' : 'nzihS7ByWBeglurZCBE3l9FAOibm8e630342-df63-4670-806f-293452da19c5';
    let installMode = codePush.InstallMode.IMMEDIATE;
    this.timeoutCodePush = setTimeout(() => {
      installMode = Platform.OS === 'ios' ? codePush.InstallMode.ON_NEXT_RESUME : codePush.InstallMode.ON_NEXT_RESTART;
      didShowApp = true;
      this.showApp();
    }, 5000);
    codePush.allowRestart();
    codePush.sync({ deploymentKey: keyTest, updateDialog: false, installMode }, (status) => {
      console.log(status);
      switch (status) {
        case codePush.SyncStatus.CHECKING_FOR_UPDATE: this.setState({ message: 'Checking update..' });
          break;
        case codePush.SyncStatus.DOWNLOADING_PACKAGE: this.setState({ message: 'Downloading package..' });
          break;
        case codePush.SyncStatus.INSTALLING_UPDATE: this.setState({ message: 'Installing update..' });
          break;
        case codePush.SyncStatus.UP_TO_DATE:
          this.setState({ message: 'Up to date..' });
          codePush.notifyApplicationReady();
          if (this.timeoutCodePush) clearTimeout(this.timeoutCodePush);
          if (didShowApp === false) this.showApp();
          break;
        case codePush.SyncStatus.UNKNOWN_ERROR:
          if (this.timeoutCodePush) clearTimeout(this.timeoutCodePush);
          if (didShowApp === false) this.showApp();
          break;
        case -1:
          if (this.timeoutCodePush) clearTimeout(this.timeoutCodePush);
          if (didShowApp === false) this.showApp();
          break;
        default:
      }
    },
    ({ receivedBytes, totalBytes }) => {
      if (this.timeoutCodePush) clearTimeout(this.timeoutCodePush);
      if (parseInt((receivedBytes * 100) / totalBytes, 10) === 100) {
        Platform.OS === 'android' && this.showRestartPrompt();
      } else {
        this.setState({
          message: `Updating ${parseInt((receivedBytes * 100) / totalBytes, 10)}%..`,
        });
      }
    });
  }
}

const styles = {
  container: {
    backgroundColor: COLOR.MAIN,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  logo: {
    width: 70,
    height: 129,
    resizeMode: 'cover',
  },
  titleContainer: {
    position: 'absolute',
    bottom: 67,
    alignSelf: 'center',
  },
  title: {
    color: 'white',
    fontSize: 36,
    fontWeight: 'bold',
  },
  descrip: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  bg: {
    flex: 1,
    width: '100%',
    height: '100%',
    resizeMode: 'stretch',
  },
};
