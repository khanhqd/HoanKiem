
export const SCREENS = {
  SPLASH: 'Splash',
  HOME_HK: 'HomeHK',
  MENU_HK: 'MenuHK',
  PROFILE_HK: 'ProfileHK',
  FOOD_DETAIL: 'FoodDetail',
  PROMOTION_DETAIL: 'PromotionDetail',
};

export const COMPONENT = {
  SPLASH                                  : require('./splash').default,
  HOME_HK                                 : require('./home').HomeHK,
  MENU_HK                                 : require('./menu').MenuHK,
  PROFILE_HK                              : require('./profile').ProfileHK,
  FOOD_DETAIL                             : require('./menu').FoodDetail,
  PROMOTION_DETAIL                        : require('./home').PromotionDetail,
};

export const FLOWCASE = {
};
