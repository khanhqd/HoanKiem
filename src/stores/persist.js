import { AsyncStorage } from 'react-native';

class Persist {
  save = async (value, asyncStoreItem) => {
    if (value === undefined || value === null) return;
    const valueType = typeof value;
    const stringValue = valueType !== 'object' && valueType !== 'array' ? String(value) : JSON.stringify(value);
    await AsyncStorage.setItem(asyncStoreItem, stringValue);
  }

  sync = async (asyncStoreItem, type) => {
    let value = await AsyncStorage.getItem(asyncStoreItem);
    if (value == null) return undefined;
    if (type === 'string') return value;
    if (type === 'number') return Number(value);
    if (type === 'object' || type === 'array') {
      try {
        let parsed = JSON.parse(value);
        return parsed;
      } catch (err) {
        return undefined;
      }
    }
    if (type === 'boolean') return value === 'true';
  }
}
export default new Persist();
