// import { create, persist } from 'mobx-persist';
// import { AsyncStorage } from 'react-native';

import UI from './UI';
import TestStore from './TestStore';

// const hydrate = create({
//   storage: AsyncStorage,
//   jsonify: true,
// });

class Store {

  // khai báo store
  ui = new UI();
  testStore = new TestStore();

  setup = async () => {
    // Setup sub-stores.
    await this.ui.setup();
  }
}

export default new Store();
