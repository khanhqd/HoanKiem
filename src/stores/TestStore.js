import { observable, autorun } from 'mobx';
import persist from './persist';

console.log('persist', typeof persist);
export default class TestStore {

  constructor() {
    this.sync();
  }

  @observable count = { value: 0 };

  @observable changeTime = [];

  sync = async () => {
    // sync with database
    let count = await persist.sync('testStore_count', 'object');
    if (count !== undefined) this.count = count;

    let changeTime = await persist.sync('testStore_changeTime', 'object');
    if (changeTime !== undefined) this.changeTime = changeTime;

    // auto save to database when changes
    autorun(() => {
      persist.save(this.count, 'testStore_count');
      persist.save(this.changeTime, 'testStore_changeTime');
    });
  }

  changeCount = (num) => {
    this.count.value += num;
    this.changeTime.push(Date.now());
  }
}

