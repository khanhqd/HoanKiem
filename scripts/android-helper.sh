#!/bin/bash

PS3='Bạn muốn làm gì: '
options=("Xem list máy ảo" "Chạy 1 máy ảo" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Xem list máy ảo")
            $ANDROID_HOME/tools/bin/avdmanager list avds
            break
            ;;
        "Chạy 1 máy ảo")
            echo "Nhập tên máy ảo (name trong list device)"
            read avdname
            $ANDROID_HOME/emulator/emulator -avd $avdname
            break
            ;;
        "Quit")
            break
            ;;
        *) echo invalid option;;
    esac
done