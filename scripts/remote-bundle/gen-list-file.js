const fs = require('fs');
const path = require('path');
const LineByLineReader = require('line-by-line');

const libLocalPath = path.join(__dirname, '../../src/libs/index.js');
const libRemotePath = path.join(__dirname, '../../src/libs/index.remote.js');
let lineContents = [];
const lr = new LineByLineReader(libLocalPath);

const startGenerate = () => {
  let newFile = 'const { remotExport } = global;\n';
  const length = lineContents.length;

  const handleLibPart = (lib) => {
    if (lib === '') return;
    let name;
    if (lib.includes(' as ')) {
      name = lib.split(' as ')[1].trim();
    } else {
      name = lib.trim();
    }
    console.log(lib, name);
    newFile += `export const ${name} = remotExport.${name};\n`;
  };

  for (let i = 0; i < length; i++) {
    const line = lineContents[i];
    if (line.includes(';') && line.includes('export') && line.includes('from')) {
      let curlyString = line.substring(line.indexOf('{'), line.length);
      curlyString = curlyString.substring(1, curlyString.indexOf('}')).trim();
      const libParts = curlyString.split(',');
      libParts.forEach(handleLibPart);
    } else if (line.includes('export') && line.includes('const')) {
      let name = line.substring(line.indexOf('const') + 5, line.length);
      name = name.substring(0, name.indexOf('=')).trim();
      newFile += `export const ${name} = remotExport.${name};\n`;
    } else {
      console.log('need handling for multiple-line export');
    }
  }

  fs.writeFileSync(libRemotePath, newFile);
};

lr.on('error', (err) => {
  console.log(err);
});

lr.on('line', (line) => {
  lineContents.push(line);
});

lr.on('end', () => {
  startGenerate();
});
