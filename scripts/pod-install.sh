#!/bin/bash

echo "Copy podscped file before install..."
#node pod-install.js
cp -R ./node_custom/react-native-contacts.podspec ./node_modules/react-native-contacts/react-native-contacts.podspec
cp -R ./node_custom/RNFirebase.podspec ./node_modules/react-native-firebase/ios/RNFirebase.podspec
cp -R ./node_custom/RNAccountKit.podspec ./node_modules/react-native-facebook-account-kit/ios/RNAccountKit.podspec
echo "Pod install"
cd ./ios
pwd
pod install
echo "Pod install done"
